//
//  GameScene.swift
//  FlappyProject
//
//  Created by Angelo J Gomez Franzin on 2/21/15.
//  Copyright (c) 2015 agomez. All rights reserved.
//

import SpriteKit

let spaceInBetweenPipes : CGFloat = 100;

class GameScene: SKScene, SKPhysicsContactDelegate {
	
	
	override func didMoveToView(view: SKView) {
		/* Setup your scene here */
		print(frame);
		canRestart = false;
		
		//Background Color
		skyColor = SKColor(red: 113.0/255, green: 197.0/255, blue: 207.0/255, alpha: 1.0);
		self.backgroundColor = skyColor;
		
		//Animation Stop-er
		moving = SKNode();
		self.addChild(moving);
		pipeParent = SKNode();
		moving.addChild(pipeParent);
		
		//World Physics
		self.physicsWorld.gravity = CGVectorMake(0, -6.0);
		self.physicsWorld.contactDelegate = self;
		
		
		////////////////////////////////////////////////
		// Bird Setup
		var birdTexture = SKTexture(imageNamed: "Bird1.png");
		birdTexture.filteringMode = SKTextureFilteringMode.Nearest;
		var birdTexture2 = SKTexture(imageNamed: "Bird2.png");
		birdTexture2.filteringMode = SKTextureFilteringMode.Nearest;
		// Flap animation
		let flap = SKAction.repeatActionForever(SKAction.animateWithTextures([birdTexture,birdTexture2], timePerFrame: 0.2))
		theBird = SKSpriteNode(texture: birdTexture);
		theBird.setScale(2.0);
		theBird.position = CGPointMake(self.frame.size.width*0.25, CGRectGetMidY(self.frame));
		theBird.runAction(flap);
		
		theBird.physicsBody = SKPhysicsBody(circleOfRadius: birdTexture.size().height);
		theBird.physicsBody?.dynamic = true;
		theBird.physicsBody?.allowsRotation  = false;
		
		
		theBird.physicsBody?.categoryBitMask = birdCategory; // 00000001
		theBird.physicsBody?.collisionBitMask = worldCategory | pipeCategory; // 00000010
		theBird.physicsBody?.contactTestBitMask = worldCategory | pipeCategory; // 0000100
		
		self.addChild(theBird);
		////////////////////////////////////////////////
		
		
		/////////////////
		// Ground
		var groundTexture = SKTexture(imageNamed: "Ground.png");
		groundTexture.filteringMode = .Nearest;
		let numberOfTimesToRepeatTexture : Int = Int(self.frame.size.width) / Int(groundTexture.size().width);
		
		//Animation
		var dx : Double = Double(groundTexture.size().width * 0.02 * 2);
		let scrollGround = SKAction.moveByX(-groundTexture.size().width*2, y: 0, duration: dx);
		let scrollGroundReset = SKAction.moveByX(groundTexture.size().width*2, y: 0, duration: 0);
		let scrollGroundForever = SKAction.repeatActionForever(SKAction.sequence([scrollGround, scrollGroundReset]));
		
		for var i = 0; i < numberOfTimesToRepeatTexture; ++i {
			var groundSprite = SKSpriteNode(texture: groundTexture);
			groundSprite.setScale(2);
			let x = groundSprite.size.width * CGFloat(i);
			groundSprite.position = CGPointMake(x, groundSprite.size.height*0.5);
			groundSprite.runAction(scrollGroundForever);
			moving.addChild(groundSprite);
		}
		
		// Ground Physics
		var groundDummy = SKNode();
		groundDummy.position = CGPointMake(0, groundTexture.size().height);
		groundDummy.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(self.frame.size.width, groundTexture.size().height*2));
		groundDummy.physicsBody?.dynamic = false;
		groundDummy.physicsBody?.categoryBitMask = worldCategory;
		self.addChild(groundDummy);
		////////////////
		
		/////////////////
		// Skyline
		var skylineTexture = SKTexture(imageNamed: "Skyline.png");
		skylineTexture.filteringMode = .Nearest;
		let numberOfTimesToRepeatTexture2 : Int = Int(self.frame.size.width) / Int(skylineTexture.size().width*2);
		
		//Animation
		dx = Double(skylineTexture.size().width * 0.1 * 2);
		let scrollSkyline = SKAction.moveByX(-skylineTexture.size().width * 2, y: 0, duration: dx);
		let scrollSkylineReset = SKAction.moveByX(skylineTexture.size().width*2, y: 0, duration: 0);
		let scrollSkylineForever = SKAction.repeatActionForever(SKAction.sequence([scrollSkyline, scrollSkylineReset]));
		
		for var i = 0; i < 2 + numberOfTimesToRepeatTexture2; ++i {
			var skylineTexture = SKSpriteNode(texture: skylineTexture);
			skylineTexture.setScale(2);
			skylineTexture.zPosition = -20;
			let x = skylineTexture.size.width * CGFloat(i);
			skylineTexture.position = CGPointMake(x, (skylineTexture.size.height * 0.5) + (groundTexture.size().height * 2));
			skylineTexture.runAction(scrollSkylineForever);
			moving.addChild(skylineTexture);
		}
		////////////////
		
		
		///////////////
		// Pipes
		pipeTexture1 = SKTexture(imageNamed: "Pipe1.png");
		pipeTexture1.filteringMode = SKTextureFilteringMode.Nearest;
		pipeTexture2 = SKTexture(imageNamed: "Pipe2.png");
		pipeTexture2.filteringMode = .Nearest;
		
		let distanceToMove	 = self.frame.size.width + 2 * pipeTexture1.size().width;
		let movePipes = SKAction.moveByX(-distanceToMove, y: 0, duration: Double(0.01 * distanceToMove));
		let removePipes =  SKAction.removeFromParent();
		moveAndRemovePipes = SKAction.sequence([movePipes, removePipes]);
		//////////////
		
		let spawn = SKAction.runBlock { [unowned self] () in self.spawnPipes() };
		let delay = SKAction.waitForDuration(2.0);
		let spawnDelay = SKAction.sequence([spawn, delay]);
		let spawnTheDelayForever = SKAction.repeatActionForever(spawnDelay);
		self.runAction(spawnTheDelayForever);
		
		
		//UI
		score = 0;
		scoreLabel = SKLabelNode(fontNamed: "MarkerFelt-Wide"); // Helvetica-Bold
		scoreLabel.position = CGPointMake(CGRectGetMidX(frame), 3 * frame.size.height / 4);
		scoreLabel.zPosition = 100;
		scoreLabel.text = "\(score)";
		addChild(scoreLabel);
		
	}
	
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		/* Called when a touch begins */
		
		if moving.speed > 0 {
			theBird.physicsBody?.velocity = CGVectorMake(0, 0)
			theBird.physicsBody?.applyImpulse(CGVectorMake(0, 7));
		} else if canRestart {
			resetGame();
		}
	}
		
	func clamp(min: CGFloat, max: CGFloat, value: CGFloat) -> CGFloat {
		if value > max {
			return max;
		} else if value < min {
			return min;
		} else {
			return value;
		}
	}
	
	override func update(currentTime: CFTimeInterval) {
		/* Called before each frame is rendered */
		if moving.speed > 0 {
			theBird.zRotation = clamp(-1, max: 1, value: theBird.physicsBody!.velocity.dy * ( theBird.physicsBody!.velocity.dy < 0 ? 0.003 : 0.001 ));
			//print(theBird.zRotation);
		}

		
	
	}
	
	func resetGame() {
		// Move bird to original position and reset velocity
		theBird.position = CGPointMake(self.frame.size.width / 4, CGRectGetMidY(self.frame));
		theBird.physicsBody?.velocity = CGVectorMake(0, 0);
		
		// Remove all existing pipes
		pipeParent.removeAllChildren();
		
		// Reset _canRestart
		canRestart = false;
		
		// Restart animation
		moving.speed = 1;
		
		score = 0;
		scoreLabel.text = "\(score)";
	}
}

extension GameScene {
	
	func spawnPipes() {
		let pipe1Node = SKNode();
		let pipe2Node = SKNode();
		pipe1Node.position = CGPointMake(self.frame.size.width + pipeTexture1.size().width, 0);
		pipe2Node.position = pipe1Node.position;
		pipe1Node.zPosition = -10;
		pipe2Node.zPosition = pipe1Node.zPosition;
		
		let y = Int(arc4random())%Int(self.frame.size.height / 3);
		let cgY = CGFloat(y + 50);
		
		let pipe1 = SKSpriteNode(texture: pipeTexture1);
		pipe1.setScale(2);
		pipe1.position = CGPointMake(0, cgY);
		pipe1.physicsBody = SKPhysicsBody(rectangleOfSize: pipe1.size);
		pipe1.physicsBody?.dynamic = false;
		pipe1.physicsBody?.categoryBitMask = pipeCategory;
		pipe1.physicsBody?.contactTestBitMask = birdCategory;
		pipe1Node.addChild(pipe1);
		
		let pipe2 = SKSpriteNode(texture: pipeTexture2);
		pipe2.setScale(2);
		pipe2.position = CGPointMake(0, cgY + pipe1.size.height + spaceInBetweenPipes);
		pipe2.physicsBody = SKPhysicsBody(rectangleOfSize: pipe2.size);
		pipe2.physicsBody?.dynamic = false;
		pipe2.physicsBody?.categoryBitMask = pipeCategory;
		pipe2.physicsBody?.contactTestBitMask = birdCategory;
		pipe2Node.addChild(pipe2);
		
		pipe1Node.runAction(moveAndRemovePipes);
		pipe2Node.runAction(moveAndRemovePipes);
		
		pipeParent.addChild(pipe1Node);
		pipeParent.addChild(pipe2Node);
		
		
		//Node for Scoring
		let scoreNode = SKNode();
		scoreNode.position = CGPointMake(self.frame.size.width + pipeTexture1.size().width + theBird.size.width, CGRectGetMidY(self.frame));
		scoreNode.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(pipe2.size.width, self.frame.height));
		scoreNode.physicsBody?.dynamic = false;
		scoreNode.physicsBody?.categoryBitMask = scoreCategory;
		scoreNode.physicsBody?.contactTestBitMask = birdCategory;
		scoreNode.runAction(moveAndRemovePipes);
		pipeParent.addChild(scoreNode);
	}
	
	func didBeginContact(contact: SKPhysicsContact) {
		if moving.speed > 0 {
			if contact.bodyA.categoryBitMask & scoreCategory == scoreCategory {
				scoreLabel.text = "\(++score)";
				scoreLabel.runAction(SKAction.sequence([SKAction.scaleTo(1.5, duration: 0.1), SKAction.scaleTo(1.0, duration: 0.1)]));
			}
			else {
				moving.speed = 0;
				removeActionForKey("Flash");
				
				let flashRedAndNormal = SKAction.sequence([SKAction.runBlock {self.backgroundColor = SKColor.redColor()}, SKAction.waitForDuration(0.05), SKAction.runBlock {self.backgroundColor = skyColor}, SKAction.waitForDuration(0.05)]);
				runAction(SKAction.sequence([SKAction.repeatAction(flashRedAndNormal, count: 4), SKAction.runBlock({canRestart = true})]), withKey: "Flash");
			}
		}
	}
}
