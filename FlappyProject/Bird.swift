//
//  Bird.swift
//  FlappyProject
//
//  Created by Angelo J Gomez Franzin on 2/21/15.
//  Copyright (c) 2015 agomez. All rights reserved.
//

import Foundation
import SpriteKit

var theBird : SKSpriteNode!
var skyColor : SKColor!

var pipeTexture1 : SKTexture!
var pipeTexture2 : SKTexture!
var moveAndRemovePipes : SKAction!
var moving : SKNode!
var pipeParent : SKNode!
var scoreLabel : SKLabelNode!

var canRestart = false;

var score = 0;

let birdCategory : UInt32 = 1 << 0;
let worldCategory : UInt32 = 1 << 1;
let pipeCategory : UInt32 = 1 << 2;
let scoreCategory: UInt32 = 1 << 3;
